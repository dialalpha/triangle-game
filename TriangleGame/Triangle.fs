﻿module Triangle

open System

/// Possible value of each coordinate in game board
type CoordinateValue =
    |Empty = 0
    |Computer = 1
    |Opponent = 2


/// Player: either Computer or Opponent (human or other machine)
type Player =
   |Computer = 0
   |Opponent = 1


/// Encapsulates a single Triangle game session
type Game = {
    board : CoordinateValue[,]
    mutable currentPlayer : Player
    mutable availableMoves : (int * int) array
    mutable currentMove : int * int // init to (-1,-1)
    mutable computerScore : int
    mutable opponentScore : int
}
with
    member this.printBoard () =
        printfn ""
        for i in 0 .. 3 do
            for j in 0 .. 3 do
                match this.board.[i,j] with
                |CoordinateValue.Computer -> printf "x\t"
                |CoordinateValue.Opponent -> printf "o\t"
                |_ -> printf "-\t"
            printfn "\n\n\n"
    
    /// compute and update scores of both Computer and Opponent
    member this.updateScores () =
        let getSquare x y=
            [|
                this.board.[x,y];
                this.board.[x + 1, y];
                this.board.[x, y + 1];
                this.board.[x + 1, y + 1]
            |]   
        let getPlayerScore player =
            let playerMark =
                match player with
                |Player.Computer -> CoordinateValue.Computer
                |_ -> CoordinateValue.Opponent
            let mutable score = 0
            for i in 0 .. 2 do
                for j in 0 .. 2 do
                    let movesInSquare = 
                        (getSquare i j)
                        |> Array.filter (fun value -> value = playerMark)
                    if movesInSquare.Length = 3 then score <- score + 1
                    elif movesInSquare.Length = 4 then score <- score + 4
            score
        this.computerScore <- getPlayerScore Player.Computer
        this.opponentScore <- getPlayerScore Player.Opponent

    /// play a move (coordinate) on the board and remove it from list of available moves
    member private this.fillCoordinate player coordinates =
        let x, y = coordinates
        match player with
        |Player.Computer -> this.board.[x,y] <- CoordinateValue.Computer
        |_ -> this.board.[x,y] <- CoordinateValue.Opponent
        this.availableMoves <- this.availableMoves |> Array.filter (fun move -> move <> this.currentMove)

    /// Returns true if there are no more moves left
    member private this.gameOver () =
        match this.availableMoves.Length with
        |0 -> true
        |_ -> false

    /// check whether a coordinate exists on the board and it is empty
    member private this.currentMoveIsValid () =
        let x, y = this.currentMove
        if x >= (Array2D.length1 this.board) ||
            y >= (Array2D.length2 this.board) then false
        elif x < 0 || y < 0 then false
        elif this.board.[x,y] <> CoordinateValue.Empty then false
        else true

    /// get opponent's move. if valid, record it on the board
    member private this.getOpponentMove () =
        printfn "Enter coordinates you'd like to play: "
        printf "Enter x coordinate: "
        let x = Console.ReadKey() 
        printfn ""
        printf "Enter y coordinate: "
        let y = Console.ReadKey()
        printfn ""
        try
            this.currentMove <- Int32.Parse(x.KeyChar.ToString()), Int32.Parse(y.KeyChar.ToString())
            if this.currentMoveIsValid() then
                this.fillCoordinate this.currentPlayer this.currentMove
                printfn "The opponent played: %A" this.currentMove
                this.currentPlayer <- Player.Computer
            else
                printfn "Invalid Move: coordinates out of bound or already filled."
        with
            |ex -> printfn "Input Exception: You must input single digits. Try again, or close window to end the game."

    /// generate and play a random move by the computer
    member private this.generateRandomComputerMove () =
        let rand = Random((int) DateTime.Now.Ticks &&& 0x0000FFFF)
        let index = rand.Next(this.availableMoves.Length)
        this.currentMove <- this.availableMoves.[index]
        this.fillCoordinate this.currentPlayer this.currentMove
        printfn "The computer played: %A" this.currentMove
        this.currentPlayer <- Player.Opponent

     /// generate and play a non-random move
    member private this.generateSmartMove () =
        let gameCopy = { this with board = Array2D.copy this.board; availableMoves = Array.copy this.availableMoves }
        // function that returns score of current player
        let scoreOfCurrentPlayer (game:Game) =
            match game.currentPlayer with
            |Player.Computer -> game.computerScore
            |_ -> game.opponentScore
        
        /// recursive loop to get computer's best current move
        /// this is possibly a blocking move.
        let rec getBiggestScoringMove bestScore bestScoringMove availableMoves =
            match availableMoves with
            |[||] -> 
                if bestScore = 0 then 
                    let rand = Random((int) DateTime.Now.Ticks &&& 0x0000FFFF)
                    let index = rand.Next(this.availableMoves.Length)
                    bestScore, this.availableMoves.[index]
                else bestScore, bestScoringMove
            |_ ->
                gameCopy.currentMove <- availableMoves.[0]
                gameCopy.fillCoordinate gameCopy.currentPlayer gameCopy.currentMove
                gameCopy.updateScores ()
                let newScore = (scoreOfCurrentPlayer gameCopy) - (scoreOfCurrentPlayer this)
                // reset board and scores
                let x, y = gameCopy.currentMove
                gameCopy.opponentScore <- this.opponentScore
                gameCopy.computerScore <- this.computerScore
                gameCopy.board.[x,y] <- CoordinateValue.Empty
                // recursive calls
                if newScore >= bestScore then // recurse with new best score
                    if availableMoves.Length > 1 then
                        getBiggestScoringMove newScore gameCopy.currentMove availableMoves.[1..]
                    else getBiggestScoringMove newScore gameCopy.currentMove [||]
                else // recurse with existing best score
                    if availableMoves.Length > 1 then
                        getBiggestScoringMove bestScore bestScoringMove availableMoves.[1..]
                    else getBiggestScoringMove bestScore bestScoringMove [||]
       
        // get computer's best move
        let computerBestScore, computerBestMove = getBiggestScoringMove 0 (-1, -1) this.availableMoves
        // get opponent's best move
        gameCopy.currentPlayer <- Player.Opponent
        let opponentBestScore, opponentBestMove = getBiggestScoringMove 0 (-1, -1) this.availableMoves
        // pick the highest scoring move and play
        if opponentBestScore > computerBestScore then
            this.currentMove <- opponentBestMove // this will block opponen't best move
        else this.currentMove <- computerBestMove
        this.fillCoordinate this.currentPlayer this.currentMove
        printfn "The computer played: %A" this.currentMove
        this.currentPlayer <- Player.Opponent
    
    /// computer will play random moves
    member this.playDumb () =
        while not (this.gameOver()) do
            match this.currentPlayer with
            |Player.Computer ->
                printfn "Computer's Turn ..."
                this.generateRandomComputerMove ()
            |_ -> 
                printfn "Opponent's Turn ..."
                this.getOpponentMove ()
            this.updateScores ()
            printfn "Scoreboard - Computer: %d - Opponnent: %d" this.computerScore this.opponentScore
            this.printBoard ()
        
   /// computer will play smarter moves
    member this.playSmart () =
        while not (this.gameOver()) do
            match this.currentPlayer with
            |Player.Computer ->
                printfn "Computer's Turn ..."
                this.generateSmartMove ()
            |_ ->
                printfn "Opponent's Turn ..."
                this.getOpponentMove ()
            this.updateScores ()
            printfn "Scoreboard - Computer: %d - Opponnent: %d" this.computerScore this.opponentScore
            this.printBoard ()
    
   
end