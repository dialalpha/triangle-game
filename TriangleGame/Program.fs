﻿open Triangle

[<EntryPoint>]
let main _ = 
    // create game session
    let newGame = {
        board = Array2D.create 4 4 CoordinateValue.Empty
        currentPlayer = Player.Computer // who goes first
        currentMove = (-1, -1)
        computerScore = 0
        opponentScore = 0
        availableMoves = 
            let moves : (int * int) array = Array.zeroCreate 16
            Array2D.zeroCreate 4 4
            |> Array2D.iteri (fun x y _ -> moves.[x*4 + y] <- x , y)
            moves
    }
    
    // play new game session
    printfn "======New Triangle Game Round======="
    newGame.playSmart ()
    printfn "\n====== Game Over ====="
    printfn "Computer Score: %d" newGame.computerScore
    printfn "Opponent Score %d" newGame.opponentScore
    printfn "\n Press any key to exit"
    System.Console.ReadKey() |> ignore
    
    0 // return an integer exit code
